class Toast {
    posX := "CENTER" ;; LEFT CENTER RIGHT
    posY := "CENTER" ;; TOP CENTER BOTTOM
    allMonitors := false
    fontSize := 11
    fontWeight := 700
    fontColor := "0xFFFFFF"
    backgroundColor := "0x1F1F1F"
    displayDuration := 1500
    fadeInDuration := 100
    fadeOutDuration := 100

    _currentToasts := {}
    _toastId := 0
    _GUI_HANDLE_PREFIX := "ToastGuiForMonitor"

    __New() {
    }

    show(msg) {
        this.close()

        DetectHiddenWindows, On

        monitors := []

        if (this.allMonitors) {
            ;; Add each monitor index to the array
            monitorN := this._getDisplayMonitorCount()
            Loop, %monitorN% {
                monitors.Push(A_Index)
            }
        } else {
            ;; Consider just the primary monitor.
            monitor := this._getPrimaryMonitor()
            monitors.Push(monitor)
        }

        INT64_MAX := 0x7FFFFFFFFFFFFFFF
        this._toastId := this._toastId < INT64_MAX ? this._toastId + 1 : 1

        for idx, monitor in monitors {
            guiId := this._showToastOnMonitor(monitor, msg)

            this._currentToasts[monitor] := guiId

        }

        if (this.displayDuration) {
            closePopups := this._autoClose.bind(this, this._toastId)
            SetTimer, % closePopups, % -this.displayDuration
        }
    }

    _autoClose(toastId) {
        if (this._toastId != toastId) {
            return
        }
        this.close()
    }

    close() {
        ;; Get monitor ids first because the for loop breaks early when
        ;; deleting keys from the object
        monitors := []
        for monitor in this._currentToasts {
            monitors.Push(monitor)
        }

        for i,monitor in monitors {
            guiId := this._currentToasts[monitor]
            this._closeToastOnMonitor(monitor)
            this._currentToasts.Delete(monitor)
        }
    }

    setPosX(posX) {
        if (posX != "TOP" && posX != "CENTER" && posX != "BOTTOM") {
            MsgBox 0x10, Invalid position, Invalid Toast position "%posX%". Falling back to "CENTER"
        }
    }

    _showToastOnMonitor(monitor, msg) {
        ; We need a different handle for each GUI in each monitor.
        GUIHandleName := this._GUI_HANDLE_PREFIX monitor

        ; Get the workspace of the monitor.
        SysGet, Workspace, MonitorWorkArea, %monitor%

        ; Greate the GUI.
        Gui, %GUIHandleName%:Destroy
        Gui, %GUIHandleName%:-Caption +ToolWindow +LastFound +AlwaysOnTop
        Gui, %GUIHandleName%:Color, % this.backgroundColor
        Gui, %GUIHandleName%:Font, % "s" this.fontSize " c" this.fontColor " w" this.fontWeight, Segoe UI ;; TODO: Make font configurable
        Gui, %GUIHandleName%:Add, Text, xp+25 yp+20, %msg%
        Gui, %GUIHandleName%:Show, Hide

        ;; TODO: What is this?
        OnMessage(0x201, "closePopups")

        ; Save the GUI ID of each GUI in a different variable.
        guiId := WinExist()

        ; Position the GUI on the monitor.
        WinGetPos, GUIX, GUIY, GUIWidth, GUIHeight
        GUIWidth += 20
        GUIHeight += 15
        if (this.posX == "LEFT") {
            NewX := WorkSpaceLeft
        } else if (this.poxX == "RIGHT") {
            NewX := WorkSpaceRight - GUIWidth
        } else {
            ; CENTER or something wrong.
            NewX := (WorkSpaceRight + WorkspaceLeft - GUIWidth) / 2
        }
        if (this.posY == "TOP") {
            NewY := WorkSpaceTop
        } else if (this.posY == "BOTTOM") {
            NewY := WorkSpaceBottom - GUIHeight
        } else {
            ; CENTER or something wrong.
            NewY := (WorkSpaceTop + WorkspaceBottom - GUIHeight) / 2
        }

        ; Show the GUI
        Gui, %GUIHandleName%:Show, Hide x%NewX% y%NewY% w%GUIWidth% h%GUIHeight%
        DllCall("AnimateWindow", "UInt", guiId, "Int", this.fadeInDuration, "UInt", "0x00080000")

        return guiId
    }

    _closeToastOnMonitor(monitor) {
        guiId := this._currentToasts[monitor]
        GUIHandleName := this._GUI_HANDLE_PREFIX monitor
        ; Fade out each toast window.
        DllCall("AnimateWindow", "UInt", guiId, "Int", this.fadeOutDuration, "UInt", "0x00090000")
        ; Free the memory used by each toast.
        Gui, %GUIHandleName%:Destroy
    }

    _getDisplayMonitorCount() {
        ;; Number of display monitors on the desktop (not including "non-display pseudo-monitors").
        SM_CMONITORS := 80
        SysGet, monitorCount, % SM_CMONITORS
        return monitorCount
    }

    _getPrimaryMonitor() {
        SysGet, primaryMonitor, MonitorPrimary
        return primaryMonitor
    }
}