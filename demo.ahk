#SingleInstance, Force
SendMode Input
SetWorkingDir, %A_ScriptDir%

#Include, src/Toast.ahk

;; Create new toast instance
t := new Toast()

;; Configure the toast style
t.posX := "CENTER" ;; LEFT CENTER RIGHT
t.posY := "CENTER" ;; TOP CENTER BOTTOM
t.allMonitors := false
t.fontSize := 11
t.fontWeight := 700
t.fontColor := "0xFFFFFF"
t.backgroundColor := "0x1F1F1F"
t.displayDuration := 1500
t.fadeInDuration := 100
t.fadeOutDuration := 100

;; Show a toast message
t.show("Hello Toast")


;; App does not exit automatically because of GUI
sleep 3000
ExitApp