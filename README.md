# Toast AHK

This library provides a class to create toasts with AutoHotkey.

The library is based on
[the Toast library](https://github.com/sdias/win-10-virtual-desktop-enhancer/blob/265b4956064184ecd8102173586ea89927b5c52a/libraries/tooltip.ahk)
by [sdias](https://github.com/sdias), but is modified to not use global
variables. It also enables configuring the toast in a central place and only
using the configured `Toast` object to show messages.

## Usage

```ahk
;; Create new toast instance
toast := new Toast()

;; Configure the toast style
toast.posX := "CENTER" ;; LEFT CENTER RIGHT
toast.posY := "CENTER" ;; TOP CENTER BOTTOM
toast.allMonitors := false
toast.fontSize := 11
toast.fontWeight := 700
toast.fontColor := "0xFFFFFF"
toast.backgroundColor := "0x1F1F1F"
toast.displayDuration := 1500
toast.fadeInDuration := 100
toast.fadeOutDuration := 100

;; Show a toast message
toast.show("Hello Toast")
```

## Limitations

1. When toasts are shown on all monitors, they fade out after each other.
   (This is because the fadeout is a blocking dll call and AutoHotkey does not
   support real multi-threading. Maybe I can somehow hack this though...)
2. New toasts from **the same** `Toast` instance override old toasts that are still visible. They do not get queued.
3. New toasts from **different** `Toast` instances do not close each other.
